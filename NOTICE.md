NOTICE
=======

Copyright(c) "Finch" contributors, all right reserved.

## Open Source License Announcement

Belows are list of open-source software used by this program. If we accidentally
missed any information, please feel free to contact us, and we will gladly
correct our mistake. To see others repositories related to Finch project, please
visit [Finch](https://gitlab.com/fsi.mob1/finch) main repository.

### Finch-Frontend: Frontend web service

- **@mdi/font**: https://www.npmjs.com/package/@mdi/font
  - Project page: https://github.com/Templarian/MaterialDesign
  - [Apache 2.0 License](doc/licenses/Apache_2.0.txt)
- **@nuxtjs/proxy**: https://www.npmjs.com/package/@nuxtjs/proxy
  - Project page: https://github.com/nuxt-community/proxy-module
  - [MIT License](doc/licenses/MIT.txt)
- **@nuxtjs/vuetify**: https://www.npmjs.com/package/@nuxtjs/vuetify
  - Project page: https://github.com/nuxt-community/vuetify-module
  - [MIT License](doc/licenses/MIT.txt)
- **axios**: https://www.npmjs.com/package/axios
  - Project page: https://axios-http.com/
  - [MIT License](doc/licenses/MIT.txt)
- **chart.js**: https://www.npmjs.com/package/chart.js
  - Project page: https://www.chartjs.org/
  - List of contributors: https://github.com/chartjs/Chart.js/graphs/contributors
  - [MIT License](doc/licenses/MIT.txt)
- **core-js**: https://www.npmjs.com/package/core-js
  - Project page: https://github.com/zloirock/core-js
  - [MIT License](doc/licenses/MIT.txt)
- **date-fns**: https://www.npmjs.com/package/date-fns
  - Project page: https://github.com/date-fns/date-fns
  - [MIT License](doc/licenses/MIT.txt)
- **dexie**: https://www.npmjs.com/package/dexie
  - Project page: https://dexie.org/
  - [Apache 2.0 License](doc/licenses/Apache_2.0.txt)
- **hchs-vue-charts**: https://www.npmjs.com/package/hchs-vue-charts
  - Project page: https://github.com/hchstera/vue-charts
  - [MIT License](doc/licenses/MIT.txt)
- **notosans-fontface**: https://www.npmjs.com/package/notosans-fontface
  - Project page: https://github.com/GreatWizard/notosans-fontface
  - [Apache 2.0 License](doc/licenses/Apache_2.0.txt) on project, [SIL Open Font License](https://www.fontsquirrel.com/license/noto-sans) on font.
- **nprogress**: https://www.npmjs.com/package/nprogress
  - Project page: https://github.com/rstacruz/nprogress
  - [MIT License](doc/licenses/MIT.txt)
- **nuxt**: https://www.npmjs.com/package/nuxt
  - Project page: https://github.com/nuxt/nuxt.js
  - [MIT License](doc/licenses/MIT.txt)
- **vue-chartjs**: https://www.npmjs.com/package/vue-chartjs
  - Project page: https://vue-chartjs.org/
  - [MIT License](doc/licenses/MIT.txt)
- **vue-json-pretty**: https://www.npmjs.com/package/vue-json-pretty
  - Project page: https://leezng.github.io/vue-json-pretty/
  - [MIT License](doc/licenses/MIT.txt)
- **vuetify**: https://www.npmjs.com/package/vuetify
  - Project page: https://vuetifyjs.com/en/
  - [MIT License](doc/licenses/MIT.txt)
- **@vue/test-utils**: https://www.npmjs.com/package/@vue/test-utils
  - Project page: https://github.com/vuejs/vue-test-utils
  - [MIT License](doc/licenses/MIT.txt)
- **babel-core**: https://www.npmjs.com/package/babel-core
  - Project page: https://babeljs.io/
  - [MIT License](doc/licenses/MIT.txt)
- **babel-jest**: https://www.npmjs.com/package/babel-jest
  - Project page: https://github.com/facebook/jest
  - [MIT License](doc/licenses/MIT.txt)
- **jest**: https://www.npmjs.com/package/jest
  - Project page: https://jestjs.io/
  - [MIT License](doc/licenses/MIT.txt)
- **sass**: https://www.npmjs.com/package/sass
  - Project page: https://github.com/sass/dart-sass
  - [MIT License](doc/licenses/MIT.txt)
- **sass-loader**: https://www.npmjs.com/package/sass-loader
  - Project page: https://github.com/webpack-contrib/sass-loader
  - [MIT License](doc/licenses/MIT.txt)
- **vue-jest**: https://www.npmjs.com/package/vue-jest
  - Project page: https://github.com/vuejs/vue-jest
  - [MIT License](doc/licenses/MIT.txt)
- **vue-material-admin**: https://vuejsprojects.com/vue-material-admin
  - Project page: https://github.com/tookit/vue-material-admin
  - Author: [Michael Wang (@tookit)](https://github.com/tookit)
  - [MIT License](doc/licenses/MIT.txt)

### Finch-Backend: Backend web service

- **flask**: https://pypi.org/project/Flask/
  - Project page: https://palletsprojects.com/p/flask
  - Author: Armin Ronacher <armin.ronacher@active-4.com>
  - [BSD License (BSD-3-Clause)](doc/licenses/BSD-3-Clause.txt)
- **gevent**: https://pypi.org/project/gevent/
  - Project page: http://www.gevent.org/
  - Author: Denis Bilenko <denis.bilenko@gmail.com>
  - [MIT License](doc/licenses/MIT.txt)
- **kafka-python**: https://pypi.org/project/kafka-python/
  - Project page: https://github.com/dpkp/kafka-python
  - Author: Dana Powers <dana.powers@gmail.com>
  - [Apache 2.0 License](doc/licenses/Apache_2.0.txt)
- **loguru**: https://pypi.org/project/loguru/
  - Project page: https://github.com/Delgan/loguru
  - Author: Delgan <delgan.py@gmail.com>
  - [MIT License](doc/licenses/MIT.txt)
- **msgpack**: https://pypi.org/project/msgpack/
  - Project page: https://msgpack.org/
  - Author: Inada Naoki <songofacandy@gmail.com> 
  - [Apache 2.0 License](doc/licenses/Apache_2.0.txt)
- **peewee**: https://pypi.org/project/peewee/
  - Project page: https://github.com/coleifer/peewee/
  - Author: Charles Leifer <coleifer@gmail.com>
  - [MIT License](doc/licenses/MIT.txt)
- **prometheus-flask-exporter**: https://pypi.org/project/prometheus-flask-exporter/
  - Project page: https://github.com/rycus86/prometheus_flask_exporter
  - Author: Viktor Adam <rycus86@gmail.com>
  - [MIT License](doc/licenses/MIT.txt)
- **psycogreen**: https://pypi.org/project/psycogreen/ 
  - Project page: https://github.com/psycopg/psycogreen/
  - Author: Daniele Varrazzo <daniele.varrazzo@gmail.com>
  - [BSD License](doc/licenses/BSD.txt)
- **psycopg2-binary**: https://pypi.org/project/psycopg2-binary/ 
  - Project page: https://psycopg.org/
  - Author: Federico Di Gregorio <fog@initd.org>
  - [GNU Library or Lesser General Public License (LGPL)](doc/licenses/LGPL-3.0.txt)
- **pytest**: https://pypi.org/project/pytest/
  - Project page: https://docs.pytest.org/en/latest/
  - Authors: Holger Krekel, Bruno Oliveira, Ronny Pfannschmidt, Floris Bruynooghe, Brianna Laugher, Florian Bruhin
    and others
  - [MIT License](doc/licenses/MIT.txt)
- **pytest-cov**: https://pypi.org/project/pytest-cov/
  - Project page: https://github.com/pytest-dev/pytest-cov
  - Author: Marc Schlaich <marc.schlaich@gmail.com>
  - [MIT License](doc/licenses/MIT.txt)
- **pytest-mock**: https://pypi.org/project/pytest-mock/
  - Project page: https://github.com/pytest-dev/pytest-mock/
  - Author: Bruno Oliveira <nicoddemus@gmail.com>
  - [MIT License](doc/licenses/MIT.txt)
- **pytest-xdist**: https://pypi.org/project/pytest-xdist/
  - Project page: https://github.com/pytest-dev/pytest-xdist
  - Author: holger krekel and contributors <pytest-dev@python.org,holger@merlinux.eu>
  - [MIT License](doc/licenses/MIT.txt)
- **pyyaml**: https://pypi.org/project/PyYAML/
  - Project page: https://pyyaml.org/
  - Author: Kirill Simonov <xi@resolvent.net>
  - [MIT License](doc/licenses/MIT.txt)
- **requests**: https://pypi.org/project/requests/
  - Project page: https://requests.readthedocs.io/
  - Author: Kenneth Reitz <me@kennethreitz.org>
  - [Apache 2.0 License](doc/licenses/Apache_2.0.txt)
- **sentry-sdk with flask extension**: https://pypi.org/project/sentry-sdk/
  - Project page: https://github.com/getsentry/sentry-python
  - Author: Sentry Team and Contributors <hello@sentry.io>
  - [BSD License](doc/licenses/BSD.txt)
- **uwsgi**: https://pypi.org/project/uWSGI/
  - Project page: https://uwsgi-docs.readthedocs.io/en/latest/
  - Author: Unbit <info@unbit.it>
  - [GPL2](doc/licenses/GPL-2.0.txt) with [Linking Exception](https://github.com/unbit/uwsgi/blob/master/LICENSE)

### Infrastructure

- **Node.JS**: https://github.com/nodejs/node
  - [Copyright Node.js contributors. All rights reserved.](https://raw.githubusercontent.com/nodejs/node/master/LICENSE)
  - [MIT License](doc/licenses/MIT.txt)
- **Python**: https://www.python.org/
  - Copyright (c) 2001-2021.  Python Software Foundation.
  - [Python Software Foundation License](doc/licenses/PSF.txt)
- **Docker Engine**: https://docs.docker.com/engine/
  - Copyright (c) 2013-2021 Docker Inc. All rights reserved.
  - [Apache 2.0 License](doc/licenses/Apache_2.0.txt)
- **Apache Kafka**: https://kafka.apache.org/
  - Author: Apache Software Foundation
  - [Apache 2.0 License](doc/licenses/Apache_2.0.txt)
- **Apache ZooKeeper**: https://zookeeper.apache.org/
  - Author: Apache Software Foundation
  - [Apache 2.0 License](doc/licenses/Apache_2.0.txt)
- **Docker image: [confluentinc/cp-kafka](https://hub.docker.com/r/confluentinc/cp-kafka/) and
  [confluentinc/cp-zookeeper](https://hub.docker.com/r/confluentinc/cp-zookeeper)**
  - Project page: https://github.com/confluentinc/kafka-images
  - [Apache 2.0 License](doc/licenses/Apache_2.0.txt)
- **PostgreSQL**: https://www.postgresql.org
  - Copyright (c) 1996-2021 The PostgreSQL Global Development Group
  - [PostgreSQL License](doc/licenses/PostgreSQL.txt)
- **Docker image: postgres**: https://hub.docker.com/_/postgres
  - Copyright (c) 2014, Docker PostgreSQL Authors
  - Maintained by: [the PostgreSQL Docker Community](https://github.com/docker-library/postgres)
  - [MIT License](doc/licenses/MIT.txt)
