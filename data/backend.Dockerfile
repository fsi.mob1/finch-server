# Finch-Server
FROM node:16-buster AS frontend
MAINTAINER limjh@fsec.or.kr

RUN apt-get update && apt-get install -y \
  python3 make g++ \
  && rm -rf /var/lib/apt/lists/*

COPY frontend /opt/
WORKDIR /opt
RUN npm install
RUN npm run build
RUN npm run generate


FROM python:3.9-buster
RUN mkdir -p /opt/static/package
RUN chmod 0775 /opt/static/package && \
    chgrp nogroup /opt/static/package
COPY --from=frontend /opt/dist/ /opt/static/

ADD backend/requirements.txt /tmp/
RUN pip3 install -r /tmp/requirements.txt
COPY backend /opt/backend/
WORKDIR /opt/backend/
RUN ln -s /opt/static
RUN touch /opt/__init__.py

WORKDIR /opt
USER nobody
EXPOSE 5000
CMD ["./backend/entrypoint.sh"]
