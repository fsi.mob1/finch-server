#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
  CREATE USER finch with encrypted password '$FINCH_DB_PASSWORD';
  CREATE DATABASE finch;
  GRANT ALL PRIVILEGES ON DATABASE finch TO finch;
EOSQL
