FROM python:3.9-buster
MAINTAINER limjh@fsec.or.kr

# process manager
ENV TINI_VERSION v0.19.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--"]

# Android platform tools
ARG ANDROID_COMPILE_SDK=29
ARG ANDROID_SDK_TOOLS=6200805
ENV ANDROID_BUILD_TOOLS=29.0.3
ENV ANDROID_HOME=/srv/android-sdk-linux

RUN apt-get update && \
    apt-get install -qy \
            openjdk-11-jre && \
    rm -rf /var/lib/apt/lists/*
RUN wget --quiet --output-document=/tmp/android-cmdline.zip \
    https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_TOOLS}_latest.zip && \
    mkdir -p ${ANDROID_HOME}/cmdline-tools && \
    unzip -qq -d ${ANDROID_HOME}/cmdline-tools /tmp/android-cmdline.zip && \
    rm /tmp/android-cmdline.zip
RUN echo y | ${ANDROID_HOME}/cmdline-tools/tools/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}" > /dev/null
RUN echo 'export PATH="$PATH:${ANDROID_HOME}/platform-tools:${ANDROID_HOME}/build-tools/${ANDROID_BUILD_TOOLS}"' >> /etc/bash.bashrc

# main dependency
COPY chain/requirements.txt /tmp/
RUN pip install -r /tmp/requirements.txt
COPY chain /opt/chain/
COPY backend/model.py /opt/chain/

EXPOSE 5000

WORKDIR /opt/
USER nobody
CMD ["python3", "-m", "chain.main"]
