# -*- coding: utf-8 -*-

import datetime
import heapq

import msgpack
import flask
from loguru import logger

from .. import model
from ..util import get_session

bp = flask.Blueprint('device', __name__)


@bp.before_app_first_request
def init_config():
    config_defaults = {
        'DEVICE_SESSION_TIMEOUT': 10*60,
    }

    for key, value in config_defaults.items():
        if key not in flask.current_app.config:
            flask.current_app.config[key] = value


@bp.route('/register', methods=['POST'])
def bp_session_register():
    req = flask.request.get_json(force=True)

    # Input verification
    required_device_keys = [
        c for c in model.Device()._meta.columns  # noqa
        if c != 'id'
    ]
    try:
        for k in required_device_keys:  # type: str
            assert type(req['device'][k]) == str

        assert type(req['applications']) == list
        for p in req['applications']:
            assert type(p['name']) == str
            assert type(p['uid']) == int
            assert type(p['hash']) == str
    except (KeyError, AssertionError) as e:
        return flask.abort(400)

    device_expr = {k: req['device'][k] for k in required_device_keys}
    device, _ = model.Device.get_or_create(**device_expr)  # type: model.Device
    session = model.Session.create(
        device_id=device,
        applications=msgpack.packb(req['applications'])
    )

    res = model.Session.update(
        status=model.SessionProcessCategory.DONE,
    ).where(
        model.Session.id != session.id,
        model.Session.device_id == session.device_id,
    ).execute()
    logger.debug('Session cleanup: %r' % (res,))

    for p in req['applications']:
        app, _ = model.Application.get_or_create(digest=p['hash'])

    return {
        'id': session.id,
        'device_id': device.id,
    }


@bp.route('/list', methods=['GET'])
def bp_session_list():
    delta = datetime.timedelta(
        seconds=flask.current_app.config['DEVICE_SESSION_TIMEOUT'])
    device_connect_limit = datetime.datetime.now() - delta

    query = model.Session.select().join(model.Device).where(
        model.Session.status == model.SessionProcessCategory.WAITING_COMMAND,
        model.Session.device_last_connected_time >= device_connect_limit
    )  # type: list[model.Session]
    res = [
        dict(id=r.id, applications=msgpack.unpackb(r.applications),
             device=r.device_id.unpack())
        for r in query
   ]
    return {
        'count': len(res),
        'sessions': res
    }


@bp.route('/start', methods=['POST'])
@get_session
def bp_session_start(session: model.Session):
    app_info = {}
    command = []
    try:
        assert session.status == model.SessionProcessCategory.WAITING_COMMAND

        req = flask.request.get_json(force=True)
        apps = msgpack.unpackb(session.applications)
    except (AssertionError, KeyError):
        return flask.abort(400)

    target = req['application']
    heapq.heappush(command, (999, {'action': 'execute', 'package': target}))

    # Check if server need to receive APK or vise versa by querying
    found = False
    for app_info in apps:
        if app_info['name'] == req['application']:
            found = True
            break

    if found:
        app = model.Application.get(
            model.Application.digest == app_info['hash']
        )
    else:
        app = model.Application.select().where(
            model.Application.package == req['application']
        ).first()

    try:
        assert app is not None  # should be registered first
        assert found or app.path
    except AssertionError:
        return flask.abort(400)

    if app.path and not found:
        # server-only; download and execute
        assert app.package
        heapq.heappush(command,
                       (0, {'action': 'download', 'uri': app.path,
                            'package': app.package}))
    elif not app.path and found:
        # client-only; upload and execute
        heapq.heappush(command,
                       (0, {'action': 'upload', 'package': target}))

    session.command = msgpack.packb([c[-1] for c in command])
    session.status = model.SessionProcessCategory.ACTIVE
    session.save()
    return {}


@bp.route('/pull', methods=['GET'])
@get_session
def bp_session_pull(session: model.Session):
    ret = {'command': None}

    if session.status != model.SessionProcessCategory.ACTIVE:
        ret['error'] = True
        ret['description'] = 'Inactive session'
        return ret

    session.device_last_connected_time = datetime.datetime.now()

    if session.command is None:
        ret['error'] = True
        ret['description'] = 'Command empty'
        session.status = model.SessionProcessCategory.DONE
        session.save()
        return ret

    if session.command_start_time is None:
        session.command_start_time = datetime.datetime.now()

    ret = {
        'command': msgpack.unpackb(session.command),
        'error': False,
    }

    session.save()
    return ret


@bp.route('/stop', methods=['GET'])
@get_session
def bp_session_stop(session: model.Session):
    try:
        assert session.status == model.SessionProcessCategory.ACTIVE
    except AssertionError:
        return flask.abort(400)

    session.command_end_time = datetime.datetime.now()
    session.status = model.SessionProcessCategory.DONE
    session.save()

    return {}
