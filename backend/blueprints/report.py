# -*- coding: utf-8 -*-

import os
import shutil
import tempfile
import gzip

from loguru import logger as log
import flask
import msgpack

from .. import model
from ..util import get_file_digest, get_application

bp = flask.Blueprint('report', __name__)


@bp.route('/start', methods=['POST'])
def bp_report_start():
    # Handle file upload
    try:
        assert 'file' in flask.request.files
        f = flask.request.files['file']
        assert f.filename
    except AssertionError:
        return flask.abort(400)

    log.info('Report starting')
    package_uri = flask.current_app.config['PACKAGE_STATIC_FOLDER_URI']
    with tempfile.NamedTemporaryFile(delete=False) as tf:
        f.save(tf.name)
        digest = get_file_digest(tf.name)
        filename = digest + '.apk'
        upload_dir = os.path.join(
            flask.current_app.config['STATIC_FOLDER'],
            package_uri.lstrip(os.path.sep)
        )
        if not os.path.exists(upload_dir):
            os.makedirs(upload_dir)

        dst = os.path.join(upload_dir, filename)
        log.info('Uploading apk to %r' % (dst,))
        if not os.path.exists(dst):
            shutil.move(tf.name, dst)

    # Prepare report entries to track progress
    # TODO: define string which represents how to download raw app file
    file_uri = '%s/%s' % (package_uri, filename)
    app, _ = model.Application.get_or_create(digest=digest)
    if app.path:
        return {'app_digest': app.digest, 'status': 'AlreadyExist'}
    else:
        app.path = file_uri
        app.save()

    log.debug('app.path=%r' % (file_uri,))
    reports = [model.ReportStatic(target=app,
                                  status=model.ReportProcessCategory.PENDING,
                                  report_type=t)
               for t in model.ReportCategory]
    model.ReportStatic.bulk_create(reports)

    # Trigger
    k = flask.current_app.config['kafka']
    k.send('cmd_report', value={'file_uri': file_uri, 'target': app.id})
    k.flush()

    return {'app_digest': app.digest, 'status': 'OK'}


@bp.route('/status', methods=['GET'])
@get_application
def bp_report_status(app: model.Application):
    reports = app.reports

    stat = {'total': 0}
    for r in reports:
        key = model.ReportProcessCategory(r.status).name.lower()
        stat[key] = stat.get(key, 0) + 1
        stat['total'] += 1

    ended = stat.get(model.ReportProcessCategory.DONE.name.lower(), 0) + \
            stat.get(model.ReportProcessCategory.ERROR.name.lower(), 0)
    stat['progress'] = ended / stat['total']

    return stat


@bp.route('/get', methods=['GET'])
@get_application
def bp_report_get(app: model.Application):
    reports = app.reports
    ret = {}
    for r in reports:  # type: model.ReportStatic
        key = model.ReportCategory(r.report_type).name.lower()
        if r.status == model.ReportProcessCategory.ERROR:
            ret[key] = 'ERROR'
        elif r.status != model.ReportProcessCategory.DONE:
            continue

        ret[key] = r.unpack()['value']

    return ret


@bp.route('/list', methods=['GET'])
def bp_report_list():
    res = {}
    for app in model.Application.select():
        res[app.digest] = [r.unpack() for r in app.reports]

    return res
