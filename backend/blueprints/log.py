# -*- coding: utf-8 -*-

import datetime
import json
from binascii import hexlify

import flask
from loguru import logger

from .. import model
from backend.util import get_session


bp = flask.Blueprint('log', __name__)


@bp.route("/push", methods=["PUT"])
@get_session
def bp_log_push(session: model.Session):
    try:
        data = flask.request.get_json(force=True)
        assert type(data) in (dict, list)

        if type(data) == dict:
            data = [data]

    except:  # noqa
        return flask.abort(400)

    k = flask.current_app.config['kafka']
    for d in data:
        payload = {'session': session.id, 'category': 'logcat', 'data': d}
        k.send('log_raw',
               key=str(session.id).encode(),
               value=json.dumps(payload).encode())

    return {'result': 'ok'}


@bp.route('/pull', methods=['GET'])
@get_session
def bp_log_pull(session: model.Session):
    """
    Pull session logs.

    Required args:
    - id: Session ID. see @get_session

    Optional args:
    - since (timestamp): Filter logs which is created since `since`.
    - limit (int): Number of logs to pull
    - is_internal (bool): Filter logs which was produced by targeted package.
    - category: comma-separated categories. ex: "DISCOVERY,HIDING"
    - query: any string we are interested in. ex: "file://"
    """
    try:
        since = datetime.datetime.fromtimestamp(
            float(flask.request.args.get('since', 0)))
        query_limit = int(flask.request.args.get('limit', '100'))
        assert 0 < query_limit <= 10000
        is_internal = flask.request.args.get('is_internal')
        query = flask.request.args.get('query', '')

        categories = [
            getattr(model.PatternCategory, u.upper())
            for u in flask.request.args.get('category', '').split(',') if u
        ]
    except (ValueError, AssertionError, AttributeError):
        return flask.abort(400)

    query_conditions = [
        model.Log.session_id == session,
        model.Log.create_at >= since,
    ]
    if is_internal:
        query_conditions.append(model.Log.is_internal is True)
    if categories:
        query_conditions.append(model.Pattern.category.in_(categories))
    if query:
        query_conditions.append(model.Log.data.contains(
            hexlify(query.encode()).decode()))

    logs = model.Log.select()\
        .join(model.Pattern, model.JOIN.LEFT_OUTER)\
        .where(*query_conditions)\
        .limit(query_limit)
    logger.debug('querying logs: %s' % (logs,))
    return json.dumps([l.unpack() for l in logs])  # noqa
