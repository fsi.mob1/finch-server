# -*- coding: utf-8 -*-

import binascii
import os
import tempfile

from kafka import KafkaProducer
from loguru import logger as log
from prometheus_flask_exporter import PrometheusMetrics
from sentry_sdk.integrations.flask import FlaskIntegration
from werkzeug.exceptions import InternalServerError
from werkzeug.middleware.dispatcher import DispatcherMiddleware
from werkzeug.wrappers import Response
import flask
import msgpack
import peewee
import sentry_sdk

from . import model
from .blueprints.log import bp as log_bp
from .blueprints.report import bp as report_bp
from .blueprints.session import bp as session_bp


sentry_dsn = os.environ.get('SENTRY_DSN')
if sentry_dsn:
    sentry_sdk.init(
        dsn=sentry_dsn,
        integrations=[FlaskIntegration()],
        traces_sample_rate=1.0,
    )

app = flask.Flask(__name__, static_url_path='')
app.config['SECRET_KEY'] = os.environ.get(
    'SECRET_KEY', binascii.hexlify(os.urandom(20)).decode())
app.config['STATIC_FOLDER'] = os.environ.get(
    'STATIC_FOLDER', tempfile.gettempdir())
app.config['PACKAGE_STATIC_FOLDER_URI'] = os.environ.get(
    'PACKAGE_STATIC_FOLDER_URI', '/package'
)

app.register_blueprint(log_bp, url_prefix='/log')
app.register_blueprint(report_bp, url_prefix='/report')
app.register_blueprint(session_bp, url_prefix='/session')

metric = PrometheusMetrics(app, group_by='endpoint')
metric.info('backend', 'Backend webapp')

metric.register_default(
    metric.counter(
        'by_path_counter', 'Request count by request paths',
        labels={'path': lambda: flask.request.path}
    )
)


@app.errorhandler(InternalServerError)
def handle_exception(e):
    if sentry_dsn:
        sentry_sdk.capture_exception(e.original_exception)

    log.exception(e.original_exception)
    return 'Error', 500


@app.before_first_request
def init_kafka():
    if 'kafka' in app.config:
        return

    k = KafkaProducer(bootstrap_servers=os.environ['KAFKA_SERVER'],
                      value_serializer=msgpack.packb)
    app.config['kafka'] = k


@app.before_first_request
def init_db():
    if 'db' in app.config:
        return

    app.config['db'] = peewee.PostgresqlDatabase(
        'finch', user='finch', password=os.environ['DB_PASSWORD'],
        host='database')

    model.database_proxy.initialize(app.config['db'])


@app.before_request
def prohibit_empty_value():
    for key in flask.request.args:
        if len(flask.request.args[key]) == 0:
            return flask.abort(400)


@app.route("/")
def app_index():
    return {"Hello": "World"}


app.wsgi_app = DispatcherMiddleware(
    Response('?', status=404),
    {'/api': app.wsgi_app}
)
