# -*- coding: utf-8 -*-

from gevent import monkey
monkey.patch_all()

from psycogreen.gevent import patch_psycopg
patch_psycopg()


from backend.main import app  # re-export
