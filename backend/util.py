import hashlib
import os
from functools import wraps

import flask

from backend import model


def get_file_digest(filename):
    assert os.path.exists(filename)

    h = hashlib.sha256()
    with open(filename, 'rb') as f:
        while True:
            block = f.read(4096)
            if len(block) == 0:
                break

            h.update(block)

    return h.hexdigest()


def get_session(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        try:
            session_id = flask.request.args['id']
            session = model.Session.get_by_id(session_id)
        except:  # noqa
            return flask.abort(400)

        return f(session=session, *args, **kwargs)

    return wrapper


def get_application(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        try:
            if 'app_digest' in flask.request.args:
                h = flask.request.args['app_digest']
                assert len(h) >= 8
                app = model.Application.get(
                    model.Application.digest.startswith(h)
                )  # type: model.Application
            else:
                assert False
        except (model.DoesNotExist, AssertionError):
            return flask.abort(400)

        return f(app=app, *args, **kwargs)

    return wrapper
