# -*- coding: utf-8 -*-

import enum
import datetime
import binascii
import gzip

import msgpack
from peewee import DatabaseProxy, Model, \
    CharField, DateTimeField, IntegerField, ForeignKeyField, \
    AutoField, BlobField, BooleanField, TextField
from peewee import DoesNotExist, JOIN  # noqa


class LogCategory(enum.IntEnum):
    LOGCAT_PLATFORM = enum.auto()
    SCREENSHOT = enum.auto()


class ReportCategory(enum.IntEnum):
    APP_DEFAULT_INFO = enum.auto()
    DEXDUMP = enum.auto()  # noqa
    MANIFEST = enum.auto()
    PERMISSION = enum.auto()
    CERTIFICATE = enum.auto()
    # MANIFEST_COMPONENT = enum.auto()


class ReportProcessCategory(enum.IntEnum):
    PENDING = enum.auto()
    IN_PROGRESS = enum.auto()
    ERROR = enum.auto()
    DONE = enum.auto()


class SessionProcessCategory(enum.IntEnum):
    WAITING_COMMAND = enum.auto()
    ACTIVE = enum.auto()
    DONE = enum.auto()


class PatternCategory(enum.IntEnum):
    DISCOVERY = enum.auto()
    HIDING = enum.auto()
    ATTACK = enum.auto()
    NETWORK = enum.auto()


database_proxy = DatabaseProxy()


class BaseModel(Model):
    class Meta:
        database = database_proxy


class Device(BaseModel):
    id = AutoField()
    serial = CharField(null=True)
    platform_build_id = CharField(null=True)
    platform_ver = CharField(null=True)
    product_brand = CharField(null=True)
    product_device = CharField(null=True)
    kernel_ver = CharField(null=True)

    def unpack(self):
        return {
            'serial': self.serial,
            'platform_build_id': self.platform_build_id,
            'platform_ver': self.platform_ver,
            'product_brand': self.product_brand,
            'product_device': self.product_device,
            'kernel_ver': self.kernel_ver,
        }


class Pattern(BaseModel):
    id = AutoField()
    log_category = IntegerField()  # LogCategory
    category = IntegerField()  # PatternCategory
    caller = CharField()
    payload_filter_regex = CharField(default=r'.*')
    title = CharField()


class Session(BaseModel):
    id = AutoField()
    device_id = ForeignKeyField(Device, backref='sessions')
    status = IntegerField(default=SessionProcessCategory.WAITING_COMMAND)  # SessionProcessCategory
    applications = BlobField(null=True)
    command = BlobField(null=True)
    device_last_connected_time = DateTimeField(default=datetime.datetime.now)
    command_start_time = DateTimeField(null=True)
    command_end_time = DateTimeField(null=True)


class Log(BaseModel):
    id = AutoField()
    session_id = ForeignKeyField(Session, backref='logs')
    pattern_id = ForeignKeyField(Pattern, backref='logs', null=True)
    category = IntegerField()  # LogCategory
    create_at = DateTimeField(default=datetime.datetime.now)
    data = CharField(max_length=5000)  # > max([max_logcat_entry_size])
    is_internal = BooleanField(default=False)

    def unpack(self):
        ret = {
            'id': self.id,
            'category': self.category,
            'create_at': datetime.datetime.timestamp(self.create_at),  # noqa
            'data': msgpack.unpackb(binascii.unhexlify(self.data)),  # noqa
            'pattern': None
        }

        if self.pattern_id:
            pattern = Pattern.get_by_id(self.pattern_id)
            ret['pattern'] = {
                'id': pattern.id,
                'category': PatternCategory(int(pattern.category)).name,
                'title': pattern.title
            }

        return ret


class Application(BaseModel):
    id = AutoField()
    digest = CharField(unique=True, index=True)
    path = CharField(max_length=1024, null=True)
    name = CharField(null=True)
    package = CharField(null=True)
    version_code = CharField(null=True)
    version_name = CharField(null=True)
    permissions = TextField(null=True)


class ReportStatic(BaseModel):
    id = AutoField()
    target = ForeignKeyField(Application, backref='reports')
    status = IntegerField()  # ReportProcessCategory
    report_type = IntegerField()  # ReportCategory
    report_value = BlobField(null=True)
    compressed = BooleanField(default=False)
    create_at = DateTimeField(default=datetime.datetime.now)
    done_at = DateTimeField(null=True)

    def unpack(self):
        data = self.report_value
        if data:
            if self.compressed:
                data = gzip.decompress(data)

            data = msgpack.unpackb(data)
            if type(data) == bytes:
                data = data.decode('utf-8', 'backslashreplace')

        return {
            'status': ReportProcessCategory(self.status).name.lower(),
            'type': ReportCategory(self.report_type).name.lower(),
            'value': data,
        }


tables = BaseModel.__subclasses__()


def create_tables():
    with database_proxy:
        database_proxy.create_tables(tables)


def drop_tables():
    with database_proxy:
        database_proxy.drop_tables(tables)
