#!/bin/sh
set -xe

uwsgi --http 0.0.0.0:5000 \
      --master \
      --single-interpreter \
      --workers 1 \
      --gevent 2000 \
      --thunder-lock --close-on-exec --enable-threads \
      --check-static ./static --static-index index.html \
      --module backend.patched:app \
      --harakiri 30 \
      --add-header "Access-Control-Allow-Origin: *" \
      --add-header "Access-Control-Allow-Methods: OPTIONS, GET, POST" \
      --add-header "Access-Control-Allow-Headers: Origin,Content-Type,Accept,Authorization,Access-Control-Expose-Headers,Access-Control-Allow-Origin,Access-Control-Allow-Methods,Access-Control-Expose-Headers" \
      --add-header "Access-Control-Expose_headers: Origin,Content-Type,Accept,Authorization,Access-Control-Expose-Headers,Access-Control-Allow-Origin,Access-Control-Allow-Methods,Access-Control-Expose-Headers" \
      --add-header "Connection: close"
