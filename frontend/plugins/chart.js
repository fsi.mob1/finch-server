import Vue from "vue";
import { Bar } from "vue-chartjs";
Chart.defaults.global.legend.display = false;

Vue.component("bar-chart", {
  extends: Bar,
  props: ["data", "options"],
  mounted() {
    this.renderBarChart();
  },
  computed: {
    chartData() {
      return this.data.datasets[0].data;
    }
  },
  methods: {
    renderBarChart() {
      this.renderChart({
        labels: this.data.labels,
        datasets: [
          {
            backgroundColor: this.data.datasets[0].backgroundColor,
            borderColor: this.data.datasets[0].borderColor,
            borderWidth: this.data.datasets[0].borderWidth,
            data: this.chartData
          }
        ]
      });
    }
  },
  watch: {
    chartData() {
      console.log("data changed");
      this._data._chart.destroy();
      this.renderBarChart();
    }
  }
});
