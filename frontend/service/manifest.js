import api from "@/service/api";

export default {
  getManifest: target => api.get(`/report/get?app_digest=${target}`),
  getManifestList: () => api.get(`/report/list`)
};
