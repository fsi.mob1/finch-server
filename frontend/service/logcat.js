import api from "@/service/api";

export default {
  parseLogcat: (session_id, since) => api.get(`/log/pull?id=${session_id}&since=${since}`)
};