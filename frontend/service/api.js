import axios from "axios";

const api = axios.create({
  baseURL: `http://localhost:5000/api`,
  proxyHeaders: false,
  credentials: false,
});

// // for authorization
// api.interceptors.request.use(
// 	config => {
// 		if (localStorage?.userinfo ?? false) {
// 			const { token } = JSON.parse(localStorage?.userinfo).auth
// 			config.headers['x-access-token'] = `${token}`
// 		}
// 		return config
// 	},
// 	err => Promise.reject(err)
// );

// api.interceptors.response.use((res) => {
// 	return res;
// }, (err) => {
// 	if (err?.response?.status === 401) {
// 		localStorage.removeItem('userinfo');
// 		swal.sessionExpired()
// 			.then(() => window.location = '/login');
// 	} else return Promise.reject(err);
// });

export default api;
