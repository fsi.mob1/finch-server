import api from "@/service/api";

export default {
  getSessionList: () => api.get(`/session/list`),
  startApp: (session_id, application) =>
    api.post(`/session/start?id=${session_id}`, { application }),
  stopApp: session_id => api.get(`/session/stop?id=${session_id}`)
};
