#!/bin/sh
set -ex

docker compose stop -t 0 backend
docker compose rm -f backend
docker compose build backend
docker compose up -d backend
docker compose logs -f backend

