# -*- coding: utf-8 -*-

from functools import partial
from typing import List
from binascii import hexlify
import time
import json

from test.fixture import *
from chain import main


class MockRecord(object):
    def __init__(self, key: str, value: dict):
        self.key = key.encode()
        value['data']['datetime'] = time.time()
        self.value = json.dumps(value).encode()


def mock_setup_consumer(_: List[str], init_data=[], **kwargs):
    return MockKafkaConsumer('mock_topic', init_data)


def mock_setup_producer():
    return MockKafkaProducer()


def test_setup_producer_mocked(mocker):
    mocker.patch('chain.main.message_queue.setup_producer', mock_setup_producer)
    producer = main.message_queue.setup_producer()
    assert type(producer) == MockKafkaProducer


def test_setup_consumer_mocked(mocker):
    expect = 123

    my_setup_consumer = partial(mock_setup_consumer, init_data=[expect])
    mocker.patch('chain.main.message_queue.setup_consumer', my_setup_consumer)

    consumer = main.message_queue.setup_consumer(['log_raw'])
    ret = consumer.poll()
    assert ret[list(ret.keys())[0]] == expect


def mock_chain_log_platform(mocker, database, consumer_init_data):
    mocker.patch('chain.main.time.sleep', lambda _: _)
    mocker.patch('chain.main.database.init',
                 lambda: main.model.database_proxy.initialize(database))
    my_setup_consumer = partial(
        mock_setup_consumer,
        init_data=consumer_init_data
    )
    mocker.patch('chain.main.message_queue.setup_consumer', my_setup_consumer)


def test_chain_log_platform_null(mocker, database):
    mock_chain_log_platform(mocker, database, [])

    with pytest.raises(ValueError):
        main.chain_log_platform()


def test_chain_log_platform_write_contents_to_database(mocker, database):
    mock_chain_log_platform(mocker, database, [[
        MockRecord(key='1',
                   value=dict(data=dict(
                       uid=n, caller='caller', payload='PAYLOAD')))
        for n in range(2)
    ]])

    with pytest.raises(ValueError):
        main.chain_log_platform()

    logs = list(main.model.Log.select())
    assert len(logs) == 2
    assert hexlify(b'PAYLOAD').decode() in logs[0].data
