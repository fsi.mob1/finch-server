# -*- coding: utf-8 -*-

import binascii
import os

import pytest
from peewee import SqliteDatabase

from backend import model
from backend.main import app


class MockKafkaProducer(object):
    def __init__(self, *args, **kwargs):
        self._sent_contents = {}

    def send(self, topic, **kwargs):
        l = self._sent_contents.get(topic, [])  # noqa
        l.append(kwargs)
        self._sent_contents[topic] = l

    def flush(self):
        pass


class MockKafkaConsumer(object):
    def __init__(self, topic, data):
        self.topic = topic
        self.buffer = data[:]

    def poll(self, *args, **kwargs):  # noqa
        if len(self.buffer) == 0:
            raise ValueError

        ret = {self.topic: self.buffer[0]}
        self.buffer = self.buffer[1:]
        return ret


@pytest.fixture()
def database():
    with SqliteDatabase(':memory:') as database:
        model.database_proxy.initialize(database)
        model.create_tables()

        yield database


@pytest.fixture
def client_with_config(database):
    os.environ['SECRET_KEY'] = binascii.hexlify(os.urandom(20)).decode()
    app.config['TESTING'] = True
    app.config['db'] = database
    app.config['kafka'] = MockKafkaProducer()
    app.config['DEVICE_SESSION_TIMEOUT'] = 10

    with app.test_client() as client:
        yield [client, app.config]


@pytest.fixture()
def client(client_with_config):
    yield client_with_config[0]
