# -*- coding: utf-8 -*-

import json

import msgpack
import pytest

from test.backend.setup import session_register
from test.fixture import *


def test_log_push_pass_to_queue(client):
    expect_key = 'foo'
    expect_val = 'bar'

    session_id = session_register(client).json['id']

    res = client.put('/api/log/push?id=%s' % (session_id,),
                     json={expect_key: expect_val})
    assert res.status_code == 200

    producer = app.config['kafka']
    received_logs = producer._sent_contents['log_raw']
    assert len(received_logs) == 1
    assert expect_key in received_logs[0]['value'].decode()
    assert expect_val in received_logs[0]['value'].decode()


def test_log_pull(client):
    expect = {'foo': 'bar'}

    session_id = session_register(client).json['id']
    model.Log.create(session_id=session_id,
                     category=model.LogCategory.LOGCAT_PLATFORM,
                     data=binascii.hexlify(msgpack.packb(expect)))

    res = client.get('/api/log/pull?id=%s' % (session_id,))
    assert res.status_code == 200
    assert json.loads(res.data)[0]['data'] == expect


@pytest.mark.parametrize('qs', [
    'since=',
    'since=0&limit=-1',
    'since=0&limit=99999999',
])
def test_log_pull_prohibits_invalid_query(client, qs):
    session_id = session_register(client).json['id']
    res = client.get('/api/log/pull?id=%s&%s' % (session_id, qs))
    assert res.status_code == 400


def test_log_pull_with_categories(client):
    session_id = session_register(client).json['id']
    pattern_attack = model.Pattern.create(
        log_category=model.LogCategory.LOGCAT_PLATFORM,
        category=model.PatternCategory.ATTACK,
        caller='XXX',
        title='ATTACK',
    )
    pattern_hiding = model.Pattern.create(
        log_category=model.LogCategory.LOGCAT_PLATFORM,
        category=model.PatternCategory.HIDING,
        caller='XXX',
        title='HIDING',
    )
    for pattern in (pattern_attack, pattern_hiding):
        model.Log.create(
            session_id=session_id,
            category=model.LogCategory.LOGCAT_PLATFORM,
            data=binascii.hexlify(msgpack.packb({'foo': 'bar'})),
            pattern_id=pattern,
        )

    # Happy path
    res = client.get('/api/log/pull?id=%s&category=%s' % (
        session_id,
        model.PatternCategory.ATTACK.name,
    ))
    assert res.status_code == 200
    assert len(json.loads(res.data)) == 1

    # Expect error on unknown PatternCategory
    res = client.get('/api/log/pull?id=%s&category=BLABLA' % (session_id,))
    assert res.status_code != 200

    # Returns nothing if category filter failed
    res = client.get('/api/log/pull?id=%s&category=%s' % (
        session_id,
        model.PatternCategory.DISCOVERY.name,
    ))
    assert res.status_code == 200
    assert len(json.loads(res.data)) == 0

    # Returns any matches of category
    res = client.get('/api/log/pull?id=%s&category=%s,%s' % (
        session_id,
        model.PatternCategory.ATTACK.name,
        model.PatternCategory.HIDING.name,
    ))
    assert res.status_code == 200
    assert len(json.loads(res.data)) == 2


def test_log_pull_with_query(client):
    session_id = session_register(client).json['id']
    model.Log.create(session_id=session_id,
                     category=model.LogCategory.LOGCAT_PLATFORM,
                     data=binascii.hexlify(msgpack.packb({'foo': 'bar'})))

    res = client.get('/api/log/pull?id=%s&query=foo' % (session_id,))
    assert res.status_code == 200
    assert len(json.loads(res.data)) == 1

    res = client.get('/api/log/pull?id=%s&query=XXX' % (session_id,))
    assert res.status_code == 200
    assert len(json.loads(res.data)) == 0
