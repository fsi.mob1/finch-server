# -*- coding: utf-8 -*-

import datetime

from test.backend.setup import session_register
from test.fixture import *


def test_register_takes_device_info_and_application_list(client):
    expect_package_name = 'com.test'

    res = session_register(client, expect_package_name)
    assert res.status_code == 200

    res = res.json
    device = model.Device.get()
    assert res['device_id'] == device.id
    session = model.Session.get()
    assert res['id'] == session.id
    assert expect_package_name.encode() in session.applications


def test_register_deduplicates_device(client):
    res = session_register(client, 'AAAA').json
    device = model.Device.get()
    assert res['device_id'] == device.id

    res2 = session_register(client, 'BBBB').json
    assert res['device_id'] == res2['device_id']


def test_register_cleanup_previous_session(client):
    session_register(client, 'AAAA')
    res = client.get('/api/session/list').json
    assert 'AAAA' in repr(res)

    session_register(client, 'BBBB')
    res = client.get('/api/session/list').json
    assert res['count'] == 1
    assert 'AAAA' not in repr(res)
    assert 'BBBB' in repr(res)


def test_list_shows_active_session(client):
    session_id = session_register(client, 'AAAA').json['id']
    session = model.Session.get_by_id(session_id)
    res = client.get('/api/session/list').json
    assert res['count'] == 1
    assert res['sessions'][0]['id'] == session.id

    past = datetime.datetime.now() - datetime.timedelta(days=1)
    session.device_last_connected_time = past
    session.save()
    res = client.get('/api/session/list').json
    assert res['count'] == 0


def test_list_shows_device_info(client):
    session_register(client, 'AAAA')
    device = model.Device.get()

    res = client.get('/api/session/list').json
    assert res['sessions'][0]['device'] == device.unpack()


def test_start(client):
    session_id = session_register(client, 'AAAA').json['id']
    res = [
        client.post('/api/session/start?id=%s' % (session_id,),
                    json={'application': 'AAAA'}).status_code
        for _ in range(2)
    ]
    assert res[0] == 200
    assert res[1] != 200  # duplicated start


def test_pull_and_stop(client):
    expect = 'com.test'

    res = client.get('/api/session/stop?id=1234')
    assert res.status_code == 400
    res = client.get('/api/session/pull?id=1234')
    assert res.status_code == 400

    session_id = session_register(client, expect).json['id']
    client.post('/api/session/start?id=%s' % (session_id,),
                json={'application': expect})

    res = client.get('/api/session/pull?id=%s' % (session_id,))
    assert res.status_code == 200
    assert len(res.json['command']) == 2
    assert res.json['command'][0]['action'] == 'upload'
    assert res.json['command'][-1]['action'] == 'execute'
    assert res.json['command'][-1]['package'] == expect
    assert not res.json['error']

    res = client.get('/api/session/stop?id=%s' % (session_id,))
    assert res.status_code == 200
    res = client.get('/api/session/pull?id=%s' % (session_id,))
    assert res.json['error']
