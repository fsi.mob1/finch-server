# -*- coding: utf-8 -*-

import json
import msgpack
import datetime

from test.fixture import *


def report_start(client):
    return client.post('/api/report/start', data={
        'file': (open(os.path.join('test', 'F-Droid.apk'), 'rb'),
                 'F-Droid.apk'),
    })


# backend/blueprints/report.py
def test_report_start(client):
    res = report_start(client)
    assert res.status_code == 200
    res_body = json.loads(res.data)
    assert res_body['status'] == 'OK'


def test_report_start_creates_db_entries(client):
    report_start(client)

    app = model.Application.get()  # noqa
    assert len(app.digest) > 0
    reports = list(model.ReportStatic.select().where(
        model.ReportStatic.target == app
    ))
    assert len(reports) == len(list(model.ReportCategory))


def test_report_start_creates_mq_entries(client_with_config):
    client, config = client_with_config
    report_start(client)
    app = model.Application.get()  # noqa

    producer = config['kafka']
    sent_report = producer._sent_contents['cmd_report']
    data = sent_report[0]['value']
    assert data['target'] == app.id


def test_report_get_by_hash(client):
    report_start(client)
    app_digest = model.Application.get().digest

    res = client.get('/api/report/get?app_digest=%s' % (app_digest,))
    assert res.status_code == 200
    res = client.get('/api/report/get?app_digest=%s' % (app_digest[:8],))
    assert res.status_code == 200
    res = client.get('/api/report/get?app_digest=%s' % (app_digest[:2],))
    assert res.status_code == 400
    res = client.get('/api/report/get')
    assert res.status_code == 400


def update_report(application: model.Application,
                  report_type: model.ReportCategory,
                  report_value: dict) -> model.ReportStatic:
    report = model.ReportStatic.select().where(
        model.ReportStatic.target == application,
        model.ReportStatic.report_type == report_type
    ).first()
    report.report_value = msgpack.packb(report_value)
    report.status = model.ReportProcessCategory.DONE
    report.done_at = datetime.datetime.now()
    report.save()

    return report


def test_report(client):
    report_type = model.ReportCategory.APP_DEFAULT_INFO
    report_value = {'foo': 'bar'}

    report_start(client)
    app = model.Application.get()
    update_report(app, report_type, report_value)

    res = client.get('/api/report/get?app_digest=%s' % (app.digest,))
    assert res.status_code == 200
    assert res.json[report_type.name.lower()] == report_value


def test_report_list(client):
    report_type = model.ReportCategory.APP_DEFAULT_INFO
    report_value = {'foo': 'bar'}

    report_start(client)
    app = model.Application.get()
    update_report(app, report_type, report_value)

    res = client.get('/api/report/list')
    assert res.status_code == 200
    assert app.digest in res.json
    assert any([r['value'] == report_value for r in res.json[app.digest]])
