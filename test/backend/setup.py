# -*- coding: utf-8 -*-
import hashlib


test_device_info = {
    'serial': 'TEST_SERIAL',
    'platform_build_id': 'TEST_PLATFORM_BUILD_ID',
    'platform_ver': 'TEST_PLATFORM_VER',
    'product_brand': 'TEST_PRODUCT_BRAND',
    'product_device': 'TEST_PRODUCT_DEVICE',
    'kernel_ver': 'TEST_KERNEL_VER',
}
test_app_uid = 10000
test_app_hash = hashlib.sha256(b'AAAA').hexdigest()


def session_register(client, expect_package_name='com.test'):
    return client.post('/api/session/register', json={
        'device': test_device_info,
        'applications': [{
            'name': expect_package_name,
            'uid': test_app_uid,
            'hash': test_app_hash,
        }]
    })
