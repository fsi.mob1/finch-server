# -*- coding: utf-8 -*-

from test.fixture import *  # noqa


# backend/main.py
def test_hello(client):
    res = client.get('/api/')
    assert res.json == {'Hello': 'World'}
