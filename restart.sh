#!/bin/sh
set -xe

docker compose stop backend chain
docker compose rm -f backend chain
docker compose build
docker compose up -d backend chain
docker compose logs -f backend chain
