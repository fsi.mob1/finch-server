# -*- coding: utf-8 -*-

import os
from receiver import event


def test_parse_one():
    sample_data = open(
        os.path.join('samples', 'sample.fdroid.20210714.bin'), 'rb').read()

    e = event.Event()
    e.read(sample_data)
    assert isinstance(e.events[0], event.EventHeader)
    assert isinstance(e.events[1], event.EventSectionNativeContext)
    assert isinstance(e.events[2], event.EventSectionMemoryRef)
