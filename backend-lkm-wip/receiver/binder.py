# -*- coding: utf-8 -*-

from enum import IntEnum
from functools import partial

from . import element, event


# Refer Kernel/include/uapi/linux/android/binder.h
class BinderConst(IntEnum):
    # Command protocol
    BC_TRANSACTION = 0x40406300
    BC_REPLY = 0x40406301
    BC_ACQUIRE_RESULT = 0x40046302
    BC_FREE_BUFFER = 0x40086303
    BC_INCREFS = 0x40046304
    BC_ACQUIRE = 0x40046305
    BC_RELEASE = 0x40046306
    BC_DECREFS = 0x40046307
    BC_INCREFS_DONE = 0x40106308
    BC_ACQUIRE_DONE = 0x40106309
    BC_ATTEMPT_ACQUIRE = 0x4008630a
    BC_REGISTER_LOOPER = 0x0000630b
    BC_ENTER_LOOPER = 0x0000630c
    BC_EXIT_LOOPER = 0x0000630d
    BC_REQUEST_DEATH_NOTIFICATION = 0x400c630e
    BC_CLEAR_DEATH_NOTIFICATION = 0x400c630f
    BC_DEAD_BINDER_DONE = 0x40086310
    BC_TRANSACTION_SG = 0x40486311
    BC_REPLY_SG = 0x40486312

    # Return protocol
    BR_ERROR = 0x80047200
    BR_OK = 0x00007201
    BR_TRANSACTION_SEC_CTX = 0x80487202
    BR_TRANSACTION = 0x80407202
    BR_REPLY = 0x80407203
    BR_ACQUIRE_RESULT = 0x80047204
    BR_DEAD_REPLY = 0x00007205
    BR_TRANSACTION_COMPLETE = 0x00007206
    BR_INCREFS = 0x80107207
    BR_ACQUIRE = 0x80107208
    BR_RELEASE = 0x80107209
    BR_DECREFS = 0x8010720a
    BR_ATTEMPT_ACQUIRE = 0x8018720b
    BR_NOOP = 0x0000720c
    BR_SPAWN_LOOPER = 0x0000720d
    BR_FINISHED = 0x0000720e
    BR_DEAD_BINDER = 0x8008720f
    BR_CLEAR_DEATH_NOTIFICATION_DONE = 0x80087210
    BR_FAILED_REPLY = 0x00007211


_binder_transaction_data_types = [
    ('target', 'uint64'),
    ('cookie', 'uint64'),
    ('code', 'uint32'),
    ('flags', 'uint32'),
    ('sender_pid', 'uint32'),
    ('sender_euid', 'uint32'),
    ('data_size', 'uint64'),
    ('offset_size', 'uint64'),
    ('buffer', 'uint64'),
    ('offsets', 'uint64'),
]


class BinderTransactionData(element.Element):
    def __init__(self):
        super().__init__(_binder_transaction_data_types)

    def __repr__(self):
        return '<BinderTransactionData target=%s code=%08x buffer=%s/%d ' \
               'offset=%s/%d>' % (
                   hex(self.target),
                   self.code,
                   hex(self.buffer), self.data_size,
                   hex(self.offsets), self.offset_size
               )


class BinderTransactionDataSg(element.Element):
    def __init__(self):
        super().__init__(_binder_transaction_data_types + [
            ('buffers_size', 'uint64')
        ])


class BinderTransactionDataSecctx(element.Element):
    def __init__(self):
        super().__init__(_binder_transaction_data_types + [
            ('secctx', 'uint64')
        ])


u32 = partial(element.FixedElement.reader_uint32, size=4)


def binder_parse(buf: bytes):
    try:
        while len(buf) >= 4:  # sizeof(u32)
            cmd = BinderConst(u32(buf))
            buf = buf[4:]
            # print(cmd.name)

            # Handle binder command/reply protocol based on cmd
            if cmd in (BinderConst.BC_TRANSACTION,
                       BinderConst.BC_REPLY,
                       BinderConst.BR_TRANSACTION,
                       BinderConst.BR_REPLY):
                binder_transaction_data = BinderTransactionData()
                binder_transaction_data.read(buf)
                buf = buf[binder_transaction_data.size:]
                print(binder_transaction_data)
                continue

            elif cmd in (BinderConst.BC_ACQUIRE_RESULT,
                         BinderConst.BC_INCREFS,
                         BinderConst.BC_ACQUIRE,
                         BinderConst.BC_RELEASE,
                         BinderConst.BC_DECREFS,
                         BinderConst.BR_ERROR,
                         BinderConst.BR_ACQUIRE_RESULT):
                buf = buf[4:]
                continue

            elif cmd in (BinderConst.BC_FREE_BUFFER,
                         BinderConst.BC_DEAD_BINDER_DONE,
                         BinderConst.BC_ATTEMPT_ACQUIRE,
                         BinderConst.BR_DEAD_BINDER,
                         BinderConst.BR_CLEAR_DEATH_NOTIFICATION_DONE):
                buf = buf[8:]
                continue

            elif cmd in (BinderConst.BC_INCREFS_DONE,
                         BinderConst.BC_ACQUIRE_DONE,
                         BinderConst.BR_INCREFS,
                         BinderConst.BR_ACQUIRE,
                         BinderConst.BR_RELEASE,
                         BinderConst.BR_DECREFS):
                buf = buf[16:]  # sizeof(struct binder_ptr_cookie)
                continue

            elif cmd in (BinderConst.BC_REGISTER_LOOPER,
                         BinderConst.BC_ENTER_LOOPER,
                         BinderConst.BC_EXIT_LOOPER,
                         BinderConst.BR_OK,
                         BinderConst.BR_DEAD_REPLY,
                         BinderConst.BR_TRANSACTION_COMPLETE,
                         BinderConst.BR_NOOP,
                         BinderConst.BR_SPAWN_LOOPER,
                         BinderConst.BR_FINISHED,
                         BinderConst.BR_FAILED_REPLY):
                continue

            elif cmd in (BinderConst.BC_REQUEST_DEATH_NOTIFICATION,
                         BinderConst.BC_CLEAR_DEATH_NOTIFICATION):
                buf = buf[12:]  # sizeof(struct binder_handle_cookie)
                continue

            elif cmd in (BinderConst.BR_ATTEMPT_ACQUIRE,):
                buf = buf[24:]  # sizeof(struct binder_pri_ptr_cookie)
                continue

            elif cmd in (BinderConst.BC_TRANSACTION_SG,
                         BinderConst.BC_REPLY_SG):
                binder_transaction_data_sg = BinderTransactionDataSg()
                binder_transaction_data_sg.read(buf)
                buf = buf[binder_transaction_data_sg.size:]
                print(binder_transaction_data_sg)
                continue

            elif cmd in (BinderConst.BR_TRANSACTION_SEC_CTX,):
                binder_transaction_data_secctx = BinderTransactionDataSecctx()
                binder_transaction_data_secctx.read(buf)
                buf = buf[binder_transaction_data_secctx.size:]
                print(binder_transaction_data_secctx)
                continue

    except Exception as e:
        print('ERROR: %r' % (e,))
        raise


def process(e: event.Event):
    # handle binder context
    try:
        event_native = e.events[1]
        assert type(event_native) == event.EventSectionNativeContext
        assert event_native.caller_type == event.CallerType.CALLER_TYPE_BINDER
        read_buffer = e.events[2].data

    except (AssertionError, IndexError):
        return

    binder_parse(read_buffer)
