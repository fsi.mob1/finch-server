# -*- coding: utf-8 -*-

import asyncio
import struct

from . import (binder, event)


async def process_event(peer: (str, int), q: asyncio.Queue[event.Event]):
    name = '%s:%d' % (peer[0], peer[1])
    print('name=%s' % (name,))

    while True:
        e = await q.get()
        q.task_done()

        binder.process(e)


async def handle_connection(
        reader: asyncio.StreamReader,
        writer: asyncio.StreamWriter):
    print('handle_connection() started')
    buffer = b''
    total_read_size = 0
    queue = asyncio.Queue()

    # Create per-event handler
    processor = asyncio.create_task(
        process_event(writer.get_extra_info('peername'), queue))

    while True:
        chunk = await reader.read(0x1000000)
        buffer += chunk

        # EOF received; cleanup connection
        if reader.at_eof():
            writer.write_eof()
            await writer.drain()
            if len(buffer) > 0:
                print('Truncating remaining %d bytes' % (len(buffer),))

            break

        # Process
        try:
            while True:
                if len(buffer) < 4:
                    break  # need more data

                e = event.Event()
                read_size = e.read(buffer)
                total_read_size += read_size
                buffer = buffer[read_size:]

                # Sanity check: before processing event, make sure client sent
                # 'complete' event object by checking next event magic. If
                # not, treat given object as corrupted.
                event_received_complete = (
                    len(buffer) == 0 or
                    (len(buffer) > 4 and
                     buffer.startswith(event.EVENT_START_MAGIC))
                )
                if event_received_complete:
                    # queue received events to other session
                    print('Event=%r progress=%d,%d,%d' % (
                        e, total_read_size - read_size, read_size, len(buffer)
                    ))
                    queue.put_nowait(e)
                else:
                    try:
                        truncate_idx = buffer.index(event.EVENT_START_MAGIC)
                    except ValueError:
                        truncate_idx = len(buffer)

                    print('Error on receiving event; dropping %d bytes' % (
                        truncate_idx,))
                    buffer = buffer[truncate_idx:]
                    total_read_size += truncate_idx

        except (IOError, struct.error):
            continue

    # Cleanup queue handler
    processor.cancel()
    await asyncio.gather(processor, return_exceptions=True)
    print('Processor terminated')


async def server_main():
    server = await asyncio.start_server(
        handle_connection, host='127.0.0.1', port=9999)

    async with server:
        await server.serve_forever()


if __name__ == '__main__':
    asyncio.run(server_main())
