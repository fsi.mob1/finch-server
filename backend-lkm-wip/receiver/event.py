# -*- coding: utf-8 -*-
from enum import IntEnum
import struct

from . import element


MAX_CMDLINE_SIZE = 256
EVENT_VERSION = 1
NATIVE_ARG_COUNT = 8
MAX_PROPERTY_SIZE = 24
MAX_EVENT_ELEM_COUNT = 32
MAX_SERIAL_SIZE = 32
TASK_COMM_LEN = 16


class CallerType(IntEnum):
    CALLER_TYPE_SYSCALL = 0
    CALLER_TYPE_BINDER = 1


class SectionMagic(IntEnum):
    HEADER = 0x434e4946
    HELLO = 0x4f4c4548
    RUNTIME_INFO = 0x434f5250
    NATIVE = 0x4954414e
    MEMORY_REF = 0x504d444d


EVENT_START_MAGIC = struct.pack('<I', SectionMagic.HEADER)


class EventBase(element.Element):
    def __init__(self, magic, items):
        super().__init__([('magic', 'uint32')] + items)
        self.magic = magic

    def read(self, buf: bytes) -> int:
        ret = super().read(buf)

        if self.magic != self.__getattr__('magic'):
            raise ValueError('Magic mismatch: expect=%08x got=%08x' % (
                self.magic, self.__getattr__('magic')
            ))

        return ret


class EventHeader(EventBase):
    def __init__(self):
        self.events = []
        super().__init__(
            SectionMagic.HEADER,  # SECTION_MAGIC_HEADER,
            [
                ('version', 'uint32'),
                ('timestamp', 'timestamp64'),
                ('number_of_sections', 'uint32'),
                ('__padding', 'uint32')
            ])


class EventSectionHello(EventBase):
    def __init__(self):
        super().__init__(
            SectionMagic.HELLO,  # SECTION_MAGIC_HELLO
            [
                ('kernel_version', 'uint32'),
                ('build_version', 'string', MAX_PROPERTY_SIZE),
                ('device_name', 'string', MAX_PROPERTY_SIZE),
                ('serial', 'string', MAX_SERIAL_SIZE),
            ]
        )


class EventSectionRuntimeInfo(EventBase):
    def __init__(self):
        super().__init__(
            SectionMagic.RUNTIME_INFO,  # SECTION_MAGIC_RUNTIME_INFO
            [
                ('pid', 'uint32'),
                ('tid', 'uint32'),
                ('uid', 'uint32'),
                ('gid', 'uint32'),
                ('program_name', 'string', TASK_COMM_LEN),
                ('cmdline', 'string', MAX_CMDLINE_SIZE),
            ]
        )


class EventSectionNativeContext(EventBase):
    def __init__(self):
        super().__init__(
            SectionMagic.NATIVE,  # SECTION_MAGIC_NATIVE
            [
                ('caller_type', 'uint8'),  # CallerType
                ('caller_major', 'uint8'),
                ('caller_minor', 'uint16'),
                ('arg_count', 'uint32'),
                ('__padding', 'uint32'),
                ('args', 'uint64', 8 * NATIVE_ARG_COUNT),
                ('ret', 'uint64'),
            ]
        )

    def __repr__(self):
        return '<EventSectionNativeContext caller=%s args=%r ret=0x%x>' % (
            self.caller_type,
            [hex(x) for x in self.args[:self.arg_count]],
            self.ret
        )


class EventSectionMemoryRef(EventBase):
    def __init__(self):
        self.data = None
        super().__init__(
            SectionMagic.MEMORY_REF,  # SECTION_MAGIC_MEMORY_REF
            [
                ('length', 'uint32'),
                ('addr', 'uint64'),
            ]
        )

    def __repr__(self):
        return '<EventSectionMemoryRef addr=0x%x length=%d>' % (
            self.addr, self.length
        )

    def read(self, buf: bytes):
        super().read(buf)
        if len(buf) < self.length + self.size:
            raise IOError('Not enough values to read data (%d < %d) ' % (
                len(buf), self.size
            ))

        self.data = buf[self.size:self.size + self.length]
        self.size += self.length


class Event(object):
    def __init__(self):
        self.events = []
        _target_events = [
            globals()[k] for k in globals()
            if k != 'EventBase' and
               type(globals()[k]) == type(EventBase) and  # noqa
               issubclass(globals()[k], EventBase)  # noqa
        ]
        self.signature_to_event_map = dict([
            (c().magic, c) for c in _target_events
        ])

    def read(self, buf: bytes) -> int:
        read_size = 0

        eh = EventHeader()
        eh.read(buf)
        read_size += eh.size
        self.events.append(eh)
        buf = buf[read_size:]

        for _ in range(eh.number_of_sections):
            magic = element.FixedElement.reader_uint32(buf, 4)
            e = self.signature_to_event_map[magic]()
            e.read(buf)
            read_size += e.size
            self.events.append(e)
            buf = buf[e.size:]

        return read_size
