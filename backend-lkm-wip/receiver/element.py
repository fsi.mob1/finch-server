# -*- coding: utf-8 -*-

import struct
import datetime
from collections import OrderedDict


class FixedElement(object):
    size_per_type = {
        'uint8': 1,
        'uint16': 2,
        'int32': 4,
        'uint32': 4,
        'int64': 8,
        'uint64': 8,
        'timestamp64': 8,
    }

    def __init__(self, _type, size=0):
        self.type = _type
        self.size = size
        self.count = 1
        self._value = None
        self._reader_func = getattr(FixedElement, 'reader_' + self.type)
        if not callable(self._reader_func):
            raise ValueError('Invalid type: %r' % (self.type,))

        _elem_size = FixedElement.size_per_type.get(_type, self.size)
        if self.size == 0:  # no size info specified; use predefined type size
            self.size = _elem_size
            assert self.size != 0
        elif self.size != _elem_size and self.size % _elem_size == 0:  # array
            self.count = self.size // _elem_size

    def __repr__(self):
        return '<FixedElement type=%s size=%d>' % (self.type, self.size)

    @staticmethod
    def _reader_using_struct(value: bytes, elem_size: int, size: int,
                             _fmt: str):
        count = size // elem_size
        ret = struct.unpack('<' + _fmt * count, value[:elem_size * count])
        if count == 1:
            ret = ret[0]

        return ret

    @staticmethod
    def reader_uint8(value: bytes, size: int) -> int:
        return FixedElement._reader_using_struct(value, 1, size, 'B')

    @staticmethod
    def reader_uint16(value: bytes, size: int) -> int:
        return FixedElement._reader_using_struct(value, 2, size, 'H')

    @staticmethod
    def reader_int32(value: bytes, size: int) -> int:
        return FixedElement._reader_using_struct(value, 4, size, 'i')

    @staticmethod
    def reader_uint32(value: bytes, size: int) -> int:
        return FixedElement._reader_using_struct(value, 4, size, 'I')

    @staticmethod
    def reader_int64(value: bytes, size: int) -> int:
        return FixedElement._reader_using_struct(value, 8, size, 'q')

    @staticmethod
    def reader_uint64(value: bytes, size: int) -> int:
        return FixedElement._reader_using_struct(value, 8, size, 'Q')

    @staticmethod
    def reader_string(value: bytes, size: int) -> str:
        return value[:size].decode().strip()

    @staticmethod
    def reader_timestamp64(value: bytes, size: int) -> datetime.datetime:
        return datetime.datetime.fromtimestamp(
            (FixedElement.reader_uint64(value, size)) / 1e9)

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, v: bytes):
        if len(v) < self.size:
            raise IOError('Not enough bytes to read attribute (%d < %d)' % (
                len(v), self.size
            ))

        self._value = self._reader_func(v, self.size)


class Element(object):
    def __init__(self, items):  # [(name, type) or (name, type, size)]
        self.size = 0
        self.attributes = OrderedDict()  # name => FixedElement

        for item in items:
            if len(item) == 2:
                name, _type = item
                size = 0
            elif len(item) == 3:
                name, _type, size = item
            else:
                raise ValueError

            e = FixedElement(_type, size)
            self.attributes[name] = e
            self.size += e.size

    def __getattr__(self, name):
        if name not in self.attributes:
            raise AttributeError('Attribute %r not defined' % (name,))

        return self.attributes[name].value

    def read(self, buf: bytes):
        if len(buf) < self.size:
            raise IOError('Not enough bytes to read (%d < %d)' % (
                len(buf), self.size))

        buf_slice = buf[:self.size]
        start_idx = 0
        for a in self.attributes.values():  # noqa
            a.value = buf_slice[start_idx:start_idx + a.size]
            start_idx += a.size
