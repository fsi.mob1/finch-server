#!/bin/sh

FILENAME=$(date '+sample.%Y%m%d%H%M%S.bin')
printf "Writing packet dump to %s\n" "${FILENAME}"
nc -l 9999 > $(date '+sample.%Y%m%d%H%M%S.bin')
