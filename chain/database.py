# -*- coding: utf-8 -*-

import os

import peewee
import yaml

from . import model


def init():
    database = peewee.PostgresqlDatabase(
        'finch', user='finch', password=os.environ['DB_PASSWORD'],
        host='database')

    model.database_proxy.initialize(database)
    if os.environ['DB_FORCE_REBUILD']:
        model.drop_tables()
        model.create_tables()
        os.environ['DB_FORCE_REBUILD'] = ''
        init_models()

    return database


def init_models():
    contents = yaml.safe_load(open(os.path.join('chain', 'init_model.yml')))

    pattern_objs = []  # noqa
    for p in contents['pattern']:
        obj = model.Pattern(
            log_category=model.LogCategory.LOGCAT_PLATFORM,
            caller=p['caller'],
            payload_filter_regex=p.get('payload_filter_regex', '.*'),
            category=getattr(model.PatternCategory, p['category'].upper()),
            title=p['title']
        )
        pattern_objs.append(obj)

    model.Pattern.bulk_create(pattern_objs)
