# -*- coding: utf-8 -*-

from functools import cache
from typing import Optional
import abc
import re

from . import model


class PatternManager(abc.ABC):
    def __init__(self):
        self.patterns = list(model.Pattern.select())

    @abc.abstractmethod
    def match(self, log: model.Log) -> Optional[model.Pattern]:
        pass


class PlatformLogPatternManager(PatternManager):
    def match(self, log: model.Log) -> Optional[model.Pattern]:
        if log.category != model.LogCategory.LOGCAT_PLATFORM:
            return None

        data = log.unpack()['data']
        for pattern in self.patterns:  # type: model.Pattern
            if data['caller'] != pattern.caller:
                continue

            if re.match(pattern.payload_filter_regex, data['payload']):
                return pattern

        return None


@cache
def platform_log_manager():
    return PlatformLogPatternManager()
