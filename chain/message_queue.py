# -*- coding: utf-8 -*-

import os
import time
from typing import List

from kafka import KafkaProducer, KafkaConsumer
from kafka.admin import KafkaAdminClient, NewTopic
from kafka.errors import NoBrokersAvailable, TopicAlreadyExistsError
import msgpack


def init(topics: List[str]):
    admin_client = None
    for _ in range(10):
        try:
            admin_client = KafkaAdminClient(
                bootstrap_servers=os.environ['KAFKA_SERVER'],
                client_id='chain_setup'
            )
            break
        except NoBrokersAvailable:
            time.sleep(1)
            continue

    assert admin_client is not None
    try:
        admin_client.create_topics(new_topics=[
            NewTopic(name=t,
                     num_partitions=1, replication_factor=1)
            for t in topics
        ], validate_only=False)
    except TopicAlreadyExistsError:
        pass

    admin_client.close()


def setup_producer():
    producer = KafkaProducer(bootstrap_servers=os.environ['KAFKA_SERVER'],
                             value_serializer=msgpack.packb)
    return producer


def setup_consumer(topics: list[str], **kwargs):
    consumer = KafkaConsumer(bootstrap_servers=os.environ['KAFKA_SERVER'],
                             value_deserializer=msgpack.unpackb,
                             **kwargs)
    consumer.subscribe(topics)
    return consumer
