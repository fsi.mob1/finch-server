# -*- coding: utf-8 -*-

import shutil
import tempfile

import requests


def download_file(url, suffix='') -> tempfile.NamedTemporaryFile:
    with tempfile.NamedTemporaryFile(delete=False, suffix=suffix) as tf:
        with requests.get(url, stream=True) as r:
            with open(tf.name, 'wb') as f:
                shutil.copyfileobj(r.raw, f)

        return tf
