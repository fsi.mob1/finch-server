# -*- coding: utf-8 -*-

from binascii import hexlify
from multiprocessing import Process
from typing import Callable, Tuple, List, Generator, Any
from functools import lru_cache
import datetime
import gzip
import json
import os
import re
import time

from loguru import logger as log
import msgpack

from . import android, model, database, message_queue, util, pattern


def init():
    message_queue.init([
        'log_raw', 'log_platform',  # log processing
        'cmd_report',  # IPC
    ])
    database.init()

    android_home = os.environ['ANDROID_HOME']
    path = os.environ['PATH'].split(os.path.pathsep)
    path += [os.path.join(*s) for s in [
        (android_home, 'platform-tools'),
        (android_home, 'build-tools', os.environ['ANDROID_BUILD_TOOLS']),
    ]]
    os.environ['PATH'] = os.path.pathsep.join(path)


@lru_cache(maxsize=1024)
def is_internal_uid(session_id: int, uid: int) -> bool:
    try:
        session = model.Session.get_by_id(session_id)  # type: model.Session
    except model.DoesNotExist:
        return True

    package = msgpack.unpackb(session.command)[-1]['package']
    apps = msgpack.unpackb(session.applications)
    log.debug('session_id=%s apps=%r' % (session_id, apps))

    for app in apps:
        if app['uid'] == uid and app['name'] == package:
            return True

    return False


def chain_log_raw():
    producer = message_queue.setup_producer()
    consumer = message_queue.setup_consumer(['log_raw'])

    for msg in consumer:
        try:
            value = json.loads(msg.value)
            assert value['data']['tag'].upper() == 'HOOKA'  # noqa
        except (AssertionError, KeyError):
            continue

        producer.send('log_platform', key=msg.key, value=msg.value)


def chain_log_platform():
    database.init()
    consumer = message_queue.setup_consumer(
        ['log_platform'], fetch_max_wait_ms=1000)
    manager = pattern.platform_log_manager()

    while True:
        messages = consumer.poll()
        if len(messages.keys()) == 0:
            continue

        messages = messages[list(messages.keys())[0]]  # we only have 1 topics
        messages_to_db = []
        for msg in messages:
            try:
                session_id = int(msg.key.decode())
                data = json.loads(msg.value)['data']
                create_at = datetime.datetime.fromtimestamp(
                    data.pop('datetime'))

                _log = model.Log(
                    session_id=session_id,
                    category=model.LogCategory.LOGCAT_PLATFORM,
                    create_at=create_at,
                    data=hexlify(msgpack.packb(data)).decode(),
                    is_internal=is_internal_uid(session_id, data['uid'])
                )
                _pattern = manager.match(_log)
                if _pattern:
                    _log.pattern_id = _pattern

                messages_to_db.append(_log)

            except Exception as e:
                log.exception(e)

        if len(messages_to_db) > 0:
            log.info('Writing %d entries to database' % (
                len(messages_to_db),))
            model.Log.bulk_create(messages_to_db)


def wrap_chain_command_handler(
        topic: str, report_type: model.ReportCategory,
        func: Callable[[str, dict], Tuple[bool, Any]]):
    database.init()
    consumer = message_queue.setup_consumer([topic])
    log.info('Handler %r started (topic=%s)' % (func, topic))

    for msg in consumer:
        data = msg.value
        log.info('%s: %r' % (report_type, data))

        report = model.ReportStatic.select().join(model.Application)\
            .where(
                model.ReportStatic.target.id == data['target'],
                model.ReportStatic.report_type == int(report_type)
            ).get()
        report.status = model.ReportProcessCategory.IN_PROGRESS
        report.save()

        apk_path = util.download_file('%s/%s' % (
            os.environ['BACKEND_SERVER'],
            data['file_uri']
        ))

        ret = {'reason': 'ERROR'}
        status = model.ReportProcessCategory.ERROR
        try:
            ok, ret = func(apk_path.name, data)
            if ok:
                status = model.ReportProcessCategory.DONE
        except Exception as e:  # noqa
            # TODO: integrate with sentry
            log.exception(e)
            pass

        apk_path.close()

        report.status = int(status)
        value = msgpack.packb(ret)
        report.report_value = value
        report.done_at = datetime.datetime.now()
        if len(value) > 1 * 1024**2:
            report.report_value = gzip.compress(value)
            report.compressed = True

        report.save()
        log.debug('done')


def handle_report_dexdump(apk_path: str, _: dict) -> (bool, bytes):  # noqa
    return True, android.dexdump(apk_path)


def chain_cmd_report_dexdump():  # noqa
    wrap_chain_command_handler('cmd_report',
                               model.ReportCategory.DEXDUMP,
                               handle_report_dexdump)


def handle_report_app_default_info(apk_path: str, data: dict) -> (bool, dict):
    app = model.Application.get_by_id(data['target'])
    ret = {}

    def _extract(source: str, key: str):
        for e in source.strip().split():
            if e.startswith(key):
                e = e[len(key):]
                e = e[e.index("'")+1:e.rindex("'")]
                yield e

    def _extract_from_list(source: List[str], key: str):
        for e in source:
            if e.startswith(key):
                e = e[len(key):]
                e = e[e.index("'")+1:e.rindex("'")]
                yield e

    def _one(l: Generator):  # noqa
        return list(l)[0]  # noqa

    info_raw_all = android.badging(apk_path).decode('utf-8', 'ignore')
    info_raw = info_raw_all.strip().split('\n')

    assert info_raw[0].startswith('package: ')
    ret['package'] = _one(_extract(info_raw[0], 'name'))
    ret['version_code'] = _one(_extract(info_raw[0], 'versionCode'))
    ret['version_name'] = _one(_extract(info_raw[0], 'versionName'))
    ret['name'] = _one(_extract_from_list(info_raw, 'application-label'))
    ret['permissions'] = list(_extract_from_list(
        info_raw, 'uses-permission: name'))
    log.debug('ret=%r' % (ret,))

    app.package = ret['package']
    app.version_code = ret['version_code']
    app.version_name = ret['version_name']
    app.name = ret['name']
    app.permissions = ','.join(ret['permissions'])
    app.save()

    return True, ret


def chain_cmd_report_app_default_info():
    wrap_chain_command_handler('cmd_report',
                               model.ReportCategory.APP_DEFAULT_INFO,
                               handle_report_app_default_info)


def handle_report_manifest(apk_path: str, _: dict) -> (bool, bytes):
    return True, android.fetch_manifest(apk_path)


def chain_cmd_report_manifest():
    wrap_chain_command_handler('cmd_report',
                               model.ReportCategory.MANIFEST,
                               handle_report_manifest)


def handle_report_permission(apk_path: str, _: dict) -> (bool, List[str]):
    return True, android.fetch_permissions(apk_path)


def chain_cmd_report_permission():
    wrap_chain_command_handler('cmd_report',
                               model.ReportCategory.PERMISSION,
                               handle_report_permission)


def handle_report_certinfo(apk_path: str, _: dict) -> (bool, List[str]):
    return True, android.certinfo(apk_path)


def chain_cmd_report_certinfo():
    wrap_chain_command_handler('cmd_report',
                               model.ReportCategory.CERTIFICATE,
                               handle_report_certinfo)


def main():
    log.info('setup')
    init()

    log.info('starting chains')
    processes = [Process(target=globals()[f], daemon=True)
                 for f in globals() if f.startswith('chain_')]
    for p in processes:
        p.start()

    for p in processes:
        p.join()


if __name__ == '__main__':
    main()
