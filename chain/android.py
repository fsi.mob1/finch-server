# -*- coding: utf-8 -*-

import os
import re
import subprocess
import tempfile
import zipfile
from typing import Callable, List

from loguru import logger as log


SIGNATURES = {
    'DEX': b'\x64\x65\x78\x0a\x30\x33',
    'ODEX': b'\x64\x65\x79\x0a\x30\x33',
}


def is_valid_split_apk(path: str) -> bool:
    with zipfile.ZipFile(path) as zf:
        return all([a.endswith('.apk') for a in zf.namelist()])


def is_valid_apk(path: str) -> bool:
    assert os.path.exists(path)

    try:
        with open(path, 'rb') as f:
            assert f.read(4) == b'PK\x03\x04'

        namelist = zipfile.ZipFile(path).namelist()
    except (AssertionError, zipfile.BadZipfile):
        log.exception('Invalid APK detected')
        return False

    # Testing 'general' APK
    try:
        for essential in (
            'AndroidManifest.xml',
            'classes.dex',
            'META-INF/MANIFEST.MF',
        ):
            assert essential in namelist

        return True
    except AssertionError:
        pass

    # Testing split APK
    try:
        assert is_valid_split_apk(path)
        return True
    except AssertionError:
        pass

    return False


def get_package_name(apk_path) -> str:
    return fetch_manifest_value(apk_path, [r'A: package="([^"]+)"'])


def get_version(apk_path) -> str:
    ret = fetch_manifest_value(
        apk_path,
        [
            r'A: android:versionCode\(0x0101021b\)=\(type 0x[0-9a-f]+\)([0-9a-fx]+)',
            r'A: android:versionCode\(0x0101021b\)=([0-9a-fx]+)'
        ])
    if ret:
        if ret.startswith('0x'):
            return str(int(ret, 16))

    return ret


def fetch_manifest_value(apk_path: str, patterns: List[str]) -> str:
    # Extract package name using aapt.
    # Since aapt don't have an option to extract package name, reference
    # AndroidManifest.xml and extract package information.
    try:
        xmltree = subprocess.check_output([
            'aapt', 'dump', 'xmltree', apk_path, 'AndroidManifest.xml'
        ]).split(b'\n')
        for line in xmltree:
            line = line.decode('utf-8').strip()
            print(line)
            for pattern in patterns:
                match = re.match(pattern, line)
                if match:
                    return match.groups()[0]
    except:
        pass

    # If target APK is split apk, extract ANY AndroidManifest.xml and grasp
    # package name from zip namelist.
    try:
        assert is_valid_split_apk(apk_path)

        with zipfile.ZipFile(apk_path) as zf:
            infolist = list(filter(lambda a: a.file_size > 0, zf.infolist()))
            infolist = sorted(infolist, key=lambda a: a.file_size)
            smallest_filename = infolist[0].filename

            with tempfile.NamedTemporaryFile() as tf:
                with zf.open(smallest_filename) as f:
                    tf.write(f.read())

                tf.flush()
                return fetch_manifest_value(tf.name, patterns)
    except:  # noqa
        log.warning('Cannot fetch manifest value from file %r' % (apk_path,))
        return ''


def fetch_manifest(apk_path: str) -> str:
    assert is_valid_apk(apk_path)
    return subprocess.check_output([
        'aapt', 'dump', 'xmltree', apk_path, 'AndroidManifest.xml'])


def fetch_permissions(apk_path: str) -> List[str]:
    assert is_valid_apk(apk_path)
    ret = subprocess.check_output(['aapt', 'dump', 'permissions', apk_path])
    return ret.decode().strip().split('\n')[1:]


def handle_analyze_split_apk(apk_path: str, func: Callable) -> dict:
    ret = {'error': 'Invalid APK'}

    if is_valid_split_apk(apk_path):
        ret = {}
        z = zipfile.ZipFile(apk_path)
        for filename in z.namelist():
            with tempfile.NamedTemporaryFile() as tf:
                tf.write(z.read(filename))
                tf.flush()

                subres = func(tf.name)
                subres_keys = subres.keys()
                for k in subres_keys:
                    ret[f'{filename}!{k}'] = subres[k]

    elif is_valid_apk(apk_path):
        ret = func(apk_path)

    return ret


def dexdump(dex_path: str) -> bytes:
    assert os.path.exists(dex_path)
    return subprocess.check_output(['dexdump', '-l', 'xml', dex_path])


def badging(apk_path: str) -> bytes:
    assert os.path.exists(apk_path)
    return subprocess.check_output(['aapt2', 'dump', 'badging', apk_path])


def certinfo(apk_path: str) -> List[str]:
    assert os.path.exists(apk_path)
    ret = subprocess.check_output(['apksigner', 'verify', '--print-certs',
                                   apk_path]).strip().split(b'\n')
    return [r.decode() for r in ret if r.startswith(b'Signer')]


def _analyze_apk_dexdump(apk_path: str) -> dict:
    ret = {}
    signature_max_length = max([len(x) for x in SIGNATURES])

    # find ALL dex files recursively, and run dexdump for each file
    z = zipfile.ZipFile(apk_path)
    for filename in z.namelist():
        try:
            signature = z.open(filename).read(signature_max_length)
        except:  # noqa
            continue

        if signature not in SIGNATURES:
            continue

        with tempfile.NamedTemporaryFile('wb', suffix='.dex') as tf:
            log.debug('DEX=%r' % (filename,))
            tf.write(z.open(filename).read())
            tf.flush()

            ret[filename] = dexdump(tf.name)

    return ret
