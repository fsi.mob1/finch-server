Finch-Server
============

## NOTICE

This project is part of [Finch](https://gitlab.com/fsi.mob1/finch); please
see earlier link for entire project description.

We may missed some requirements such as mentioning correct authors of our
dependent library, and so on. We never tries to violate other's, so if you
found mistake please feel free to contact us and we will gladly correct them.

## AUTHORS

- Jisub Kim <jskimpwn@gmail.com>
- Kyuju Kim <kuyjuim@naver.com>
- Jihyeog Lim <marnitto@gmail.com>

## LICENSE

Apache 2.0.

See [LICENSE](LICENSE) for details, and see [NOTICE](NOTICE.md)
for authors and licenses this program depends on.
